const assert = require('assert');
const commonCheck = require('../js/common-check');
const cardReq = require('../js/card-requisites');

describe('isNumber Tests', () => {
    it('Если буквы возращает false', () => assert.strictEqual(commonCheck.isNumber('ewwr'), false));
    it('Если цифры возращает true', () => assert.strictEqual(commonCheck.isNumber('1212'), true));
    it('На пустую строку возвращает false', () => assert.strictEqual(commonCheck.isNumber(''), false));
});


describe('isEmpty Tests', () => {
  it('Если строка возращает false', () => assert.strictEqual(commonCheck.isEmpty('ewwr'), false));
  it('Если пусто возращает true', () => assert.strictEqual(commonCheck.isEmpty(''), true));
  it('Если число возращает false', () => assert.strictEqual(commonCheck.isEmpty(123), false));
});

// describe('cardNumberValidate Tests', () => {
//   it('Поле валидно, только если номер карты состоит из 16 цифр.', () => assert.throws(cardReq.cardNumberValidate('ewwr')));
 
// });

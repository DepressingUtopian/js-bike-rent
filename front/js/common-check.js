export function isEmpty(text) {
  return !text;
}

export function isNumber(text) {
  return Number(text) ? true : false;
}

export function commonValidate(value) {
  if (!value)
    throw 'Поле не может быть пустым';
  if (!isNumber(value))
    throw 'Поле может содержать только цифры';
}

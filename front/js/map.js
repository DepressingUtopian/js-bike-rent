ymaps.ready(init);

function init() {
    // Создание карты.
    const map = new ymaps.Map("map", {
        // Координаты центра карты.
        // Порядок по умолчанию: «широта, долгота».
        // Чтобы не определять координаты центра карты вручную,
        // воспользуйтесь инструментом Определение координат.
        center: [55.0208377, 82.8979239],
        // Уровень масштабирования. Допустимые значения:
        // от 0 (весь мир) до 19.
        zoom: 12
    });

    const balloonContentLayoutClass =  ymaps.templateLayoutFactory.createClass(
        '<div class="balloon-layout"><h4>Пункт по адресу {{properties.address}}</h4> <button onClick="console.log(document.location.href=\'/catalog/{{properties.id}}\')">Выбрать велосипед</button> </div>'
    );

    const pointers = api.getPointers().then((result) => {
        result.forEach(element => {
            const placemark = new ymaps.Placemark(element.coordinates, {address: element.address, id: element._id}, {balloonContentLayout:balloonContentLayoutClass});
            console.log(element.coordinates);
            map.geoObjects.add(placemark);
        });
    });

}


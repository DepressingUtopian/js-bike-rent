'use strict';
import {getBikes} from './fetch.js';

let loadMoreButton;
let currentPage = 1;
function init() {
  // вызови функцию loadCatalog для загрузки первой страницы каталога
  loadCatalog(currentPage);

  // Реализуй и установи обработчик нажатия на кнопку "Загрузить еще"
  loadMoreButton = document.querySelector('#loadMore');
  if (loadMoreButton) {
    loadMoreButton.addEventListener('click', () => {
      disableButtonLoadMore();
      new Promise((resolve, reject) => {
        loadCatalog(currentPage)
        resolve();
      }).then(() => enableButtonLoadMore());

    });
  }
}

function loadCatalog(page) {
  // Здесь необходимо сделать загрузку каталога (api.getBikes)
  // и передать полученные данные в функции appendCatalog и showButtonLoadMore
  if (api) {
    console.log(getPointId() + " - " + page);
    getBikes(getPointId(), page).then((request) => {
      appendCatalog(request.bikesList);
      showButtonLoadMore(request.hasMore);
      currentPage = request.hasMore ? currentPage + 1 : 1;
    });
}

function appendCatalog(items) {
  // отрисуй велосипеды из items в блоке <div id="bikeList">
  const bikeListElement = document.querySelector('#bike-list');
  const template = document.querySelector('#bike-card__template').innerHTML;
  if (bikeListElement) {
    const rendered = Mustache.render(template, { bikeList: items });
    bikeListElement.innerHTML += rendered;
  } else {
    console.error('Функция: appendCatalog , bikeListElement - undef');
  }
}

function showButtonLoadMore(hasMore) {
  // если hasMore == true, то показывай кнопку #loadMore
  // иначе скрывай
  if (hasMore) {
    loadMoreButton.classList.remove('hidden');
  } else {
    loadMoreButton.classList.add('hidden');
  }
}

function disableButtonLoadMore() {
  if (loadMoreButton) {
    loadMoreButton.classList.add('disable');
  }
}

function enableButtonLoadMore() {
  if (loadMoreButton) {
    loadMoreButton.classList.remove('disable');
  }
}

function getPointId() {
  const url = document.URL;
  const result = url.match(/(?<=catalog\/)(\w*)/gi);
  return result ? result[0] : '';
}


document.addEventListener('DOMContentLoaded', init)

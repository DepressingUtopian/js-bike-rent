export async function sendCardRequisitesData(url, data) {
  try {
    const response = await fetch(url, {
      method: 'PUT',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
      body: JSON.stringify(data)
    });

    if (response.ok) {
      return response;
    } else {
      throw (`Код ошибки:${response.status}, ${response.statusText}`);
    }

  } catch (error) {
    console.error(error);
  }

}
const urlBikes = '/api/catalog/';
function getUrl(pointId, page) {
  let result = urlBikes;

  if (pointId) {
    result += pointId;
  }

  if (page) {
    result += '?page=' + page;
  }

  return result;
}
export async function getBikes(pointId, page) {
  const response = await fetch(getUrl(pointId, page), {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
  });

  if (response.ok) {
    return await Promise.resolve(await response.json());
  } else {
    return Promise.reject(`Код ошибки:${response.status} ${response.statusText}`);
  }
}

export async function deleteOrderById(url, orderId) {
  const response = await fetch(url + orderId, {
    method: 'DELETE',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
  });

  if (response.ok) {
    return await Promise.resolve(await response);
  } else {
    return Promise.reject(`Код ошибки:${response.status} ${response.statusText}`);
  }
}

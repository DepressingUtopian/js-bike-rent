import { deleteOrderById } from './fetch.js';

const fetchUrl = '/api/order/';

const buttons = document.querySelectorAll('.bike-card__apply-button');
buttons.forEach((element) => {element.addEventListener('click',(event) => applyButtonHandler(event))});

function applyButtonHandler(event) {
  const button = event.target;
  const orderId = button.getAttribute('data-order-id');
  if (orderId) {
    deleteOrderById(fetchUrl, orderId)
      .then((message) => event.target.parentNode.parentNode.remove())
      .catch(error => {
        console.error(error);
      });
  }
}
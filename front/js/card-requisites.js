import { isNumber, commonValidate } from './common-check.js';
import { sendCardRequisitesData } from './fetch.js';

const DATE_SEPARATOR = '/';
const PUT_URL = '/api/card-requisites';
console.log(document.referrer);
console.log(getBackUrl());

document.querySelector('#card-num').addEventListener('blur', (event) => commonHandler(event.target, cardNumberHandler));
document.querySelector('#card-date').addEventListener('blur', (event) => commonHandler(event.target, cardDateHandler));
document.querySelector('#card-cvv').addEventListener('blur', (event) => commonHandler(event.target, cardCVVHandler));

document.querySelector('.card-form').addEventListener('submit', (event) => onSubmitHandler(event));

function commonHandler(elem, func, isSubmitEvent = false) {
  try {
    if (isSubmitEvent) {
      commonValidate(deleteDateSeparator(deleteAllSpace(elem.value)));
    }

    if (elem.value) {
      func(elem);
    }

    hideError(elem);
    return true;
  } catch (error) {
    renderError(error, elem);
    return false;
  }
}

function onSubmitHandler(event) {
  const customInputs = document.querySelectorAll('.custom-input')
  const cardNumberInput = customInputs[0].firstElementChild;
  const cardDateInput = customInputs[1].firstElementChild;
  const cardCVVInput = customInputs[2].firstElementChild;

  if (commonHandler(cardNumberInput, cardNumberHandler, true) &&
    commonHandler(cardDateInput, cardDateHandler, true) &&
    commonHandler(cardCVVInput, cardCVVHandler, true)) {
    sendCardRequisitesData(PUT_URL, { date:cardDateInput.value,
         number:cardNumberInput.value, cvv:cardCVVInput.value }).then((request) => console.log(request.ok));
         const backUrl = getBackUrl();
         window.location = backUrl ? backUrl : "/lk";
        }
  event.preventDefault();
}

function cardNumberHandler(elem) {
  const cardNumberValue = deleteAllSpace(elem.value);
  cardNumberValidate(cardNumberValue);
  elem.value = cardNumberAddSpace(cardNumberValue);
}

function cardDateHandler(elem) {
  const cardDate = deleteAllNotNumber(elem.value);
  elem.value = addDateSeparator(cardDate);
  cardDateValidation(elem.value);
}

function cardCVVHandler(elem) {
  cvvValidate(elem.value);
}

function deleteAllSpace(text) {
  return text.replaceAll(' ', '');
}

function deleteDateSeparator(text) {
  return text.replaceAll(DATE_SEPARATOR, '');
}

function cardNumberValidate(cardNumberValue) {
  if (cardNumberValue) {
    const length = cardNumberValue.length;
    if (length !== 16)
      throw 'Поле должно содержать 16 цифр';
  }
}

function cardNumberAddSpace(cardNumberValue) {
  const length = cardNumberValue.length;
  let result = '';

  for (let i = 0; i <= length - 4; i += 4) {
    let number = cardNumberValue.slice(i, i + 4);
    if (isNumber(number)) {
      const space = (i < length - 4) ? ' ' : '';
      result += number + space;
    } else {
      throw 'Номер карты должен состоять из цифр';
    }
  }
  return result;
}

function getTodayNumberYear() {
  return Number(String(new Date().getFullYear()).slice(2, 4));
}

function deleteAllNotNumber(text) {
  return ([...text].map((symbol) => {
    return isNumber(symbol) || symbol === '0' ? symbol : '';
  })).join('');
}

function addDateSeparator(date) {
  return [date.slice(0, 2) + DATE_SEPARATOR + date.slice(2)]
}

function cardDateValidation(cardDateValue) {
  cardDateValue = cardDateValue.replaceAll(' ', '');
  const [month, year] = cardDateValue.split('/');
  const todayYear = getTodayNumberYear();
  if (cardDateValue) {
    if (month.length != 2 || year.length != 2)
      throw 'Номер месяца и номер года должны иметь двухзначный формат';
    if (!isNumber(month))
      throw 'Месяц должен состоять из цифр';
    if (Number(month) <= 0 || Number(month) > 12)
      throw 'Номер месяца должен существовать';
    if (!isNumber(year))
      throw 'Номер года должен состоять из цифр';
    if (Number(year) > todayYear + 5 || Number(year) < todayYear - 5)
      throw 'Не валидный срок годности карты';

  }
}

function cvvValidate(cardCvvValue) {
  const lenght = cardCvvValue.length;
  if (lenght !== 3 || !isNumber(cardCvvValue))
    throw 'CVV должно состоять из трех цифр';
}

function renderError(error, elem) {
  const tooltipElem = elem.nextSibling.nextSibling;
  const tooltipText = tooltipElem.firstElementChild.firstElementChild;
  elem.classList.add('error-input');
  tooltipElem.classList.remove('hidden');
  tooltipText.innerText = error;
}

function hideError(elem) {
  const tooltipElem = elem.nextSibling.nextSibling;
  const tooltipText = tooltipElem.firstElementChild.firstElementChild;
  elem.classList.remove('error-input');
  tooltipElem.classList.add('hidden');
  tooltipText.innerText = '';
}

function getBackUrl() {
  const index = document.URL.indexOf('back_url=');
  return (index) ? document.URL.slice(index  + 'back_url='.length).replaceAll('%2F', '/') : undefined;
}
